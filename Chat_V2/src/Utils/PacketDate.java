package Utils;

import java.io.Serializable;
import java.util.LinkedList;

public class PacketDate  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PacketType typePacket;
	private String nickName;
	private String message;
	private String ip;
	private LinkedList<String> UsersList;
	
	
	
	public PacketDate(PacketType typePacket, String nickName, String message) {
		this.setTypePacket(typePacket);
		this.nickName = nickName;
		this.message = message;
		this.ip = null;
		this.UsersList = null;
	}
	
	
	public PacketDate(PacketType typePacket, LinkedList<String> usersList) {
		this.setTypePacket(typePacket);
		UsersList = usersList;
		this.message = null;
		this.ip = null;
		this.nickName=null;
	}
	

	public PacketDate(PacketType typePacket, String nickName, String message, String ip) {
		this.typePacket = typePacket;
		this.nickName = nickName;
		this.message = message;
		this.ip = ip;
		this.UsersList = null;
	}
	

	public PacketDate(PacketType typePacket) {
		this.typePacket = typePacket;
		this.message = null;
		this.ip = null;
		this.nickName=null;
		this.UsersList = null;
	}


	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public LinkedList<String> getUsersList() {
		return UsersList;
	}
	public void setUsersList(LinkedList<String> usersList) {
		UsersList = usersList;
	}


	public PacketType getTypePacket() {
		return typePacket;
	}


	public void setTypePacket(PacketType typePacket) {
		this.typePacket = typePacket;
	}
	
	
	
}
