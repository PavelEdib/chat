package Utils;

public enum PacketType {
	TEXT_MESSAGE,
	LIST_OF_USERS,
	REQUEST_PACKET,
	REQUEST_USER,
	DISABLE_USER
}
