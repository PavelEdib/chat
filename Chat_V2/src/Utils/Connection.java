package Utils;

import java.io.*;
import java.net.Socket;

public class Connection implements Closeable {
	
    private final Socket socket;
    private final ObjectOutputStream out;
    private final ObjectInputStream in;


    public Connection(Socket socket) throws IOException {
        this.socket = socket;
        this.out = new ObjectOutputStream(socket.getOutputStream());
        this.in = new ObjectInputStream(socket.getInputStream());
    }

    
    public void send(PacketDate packet) throws IOException {
        synchronized (this.out) {
            out.writeObject(packet);
        }
    }
    
    public Socket getSocket() {
		return this.socket;
    }

    
    public PacketDate receive() throws IOException, ClassNotFoundException {
        synchronized (this.in) {
        	PacketDate packet = (PacketDate) in.readObject();
            return packet;
        }
    }

    @Override
    public void close() throws IOException {
        in.close();
        out.close();
        socket.close();
    }
}
