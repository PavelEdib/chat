package Server;

import Utils.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Map;

public class Server {
    private ServerSocket serverSocket;
    private static ViewGuiServer gui; 
    private static ModelGuiServer model; 
    private static volatile boolean isServerStart = false; 
    
   protected void startServer(int port) {
        try {
            serverSocket = new ServerSocket(port);
            isServerStart = true;
            gui.refreshDialogWindowServer("Server Started...\n");
        } catch (Exception e) {
            gui.refreshDialogWindowServer("Coudn't Start the Server.\n");
        }
    }

   protected void stopServer() {
        try {
            if (serverSocket != null && !serverSocket.isClosed()) {
                for (Map.Entry<String, Connection> user : model.getAllUsersMultiChat().entrySet()) {
                    user.getValue().close();
                }
                serverSocket.close();
                model.getAllUsersMultiChat().clear();
                gui.refreshDialogWindowServer("Server Stoped.\n");
            } else gui.refreshDialogWindowServer("Server is already Stoped.\n");
        } catch (Exception e) {
            gui.refreshDialogWindowServer("Can't stop the server.\n");
        }
    }

    protected void acceptServer() {
        while (true) {
            try {
                Socket socket = serverSocket.accept();
                new ServerThread(socket).start();
            } catch (Exception e) {
                gui.refreshDialogWindowServer("Connection Lost.\n");
                break;
            }
        }
    }

    //eviamos el pacquete a todos los usuarios de la lista (habitualmente es el paquete con la lista de usuarios online)
    protected void sendPacketAllUsers(PacketDate message) {
        for (Map.Entry<String, Connection> user : model.getAllUsersMultiChat().entrySet()) {
            try {
                user.getValue().send(message);
            } catch (Exception e) {
                gui.refreshDialogWindowServer("Error in to sending Message \n");
            }
        }
    }

    
    public static void main(String[] args) {
        Server server = new Server();
        gui = new ViewGuiServer(server);
        model = new ModelGuiServer();
        gui.initFrameServer();
        
        
        //creamos el thread que siempre esta escuchando
        while (true) {
            if (isServerStart) {
                server.acceptServer();
                isServerStart = false;
            }
        }
    }

    
    private class ServerThread extends Thread {
        private Socket socket;

        public ServerThread(Socket socket) {
            this.socket = socket;
        }
        
        
         private String requestAndAddingUser(Connection connection) {
        	 while(true) {
	           try {
					connection.send(new PacketDate(PacketType.REQUEST_USER));
					PacketDate packReciev = connection.receive();
					
					
					String ip = packReciev.getIp();
					String nombre = packReciev.getNickName();
					//Add new User
					model.addUser(ip+" "+nombre, connection);
		            LinkedList<String> listUsers = new LinkedList<>();
		            for (Map.Entry<String, Connection> users : model.getAllUsersMultiChat().entrySet()) {	   
		                listUsers.add(users.getKey());
		            }
		            
		            //pasamos lista de ip al nuevo User
		            connection.send(new PacketDate(PacketType.LIST_OF_USERS,listUsers));
		            
		            //Eviamos a todos nueva lista de usuarios(sus Ip)
		            sendPacketAllUsers(new PacketDate(PacketType.LIST_OF_USERS,listUsers));
	            return packReciev.getNickName();
			} catch (IOException | ClassNotFoundException e) {
				gui.refreshDialogWindowServer("Error in Request a User");
			}
         }
      }

      //Recibe los mensaje y se preocupa de enviar a otros
      private void messagingBetweenUsers(Connection connection,String nameUser) {
           while(true){
        	   try {
        		
				PacketDate packet = connection.receive();
				
				//si es un packet de tipo mensaje
				if(packet.getTypePacket() ==PacketType.TEXT_MESSAGE) {
					
					String nickToRecieve = packet.getIp();
					//Conseguimos el connection de Receptor
					Connection ConnToRecieve = getConnection(packet.getIp());
					
					//Conseguimos el ip del Emisor
					InetAddress  ip = socket.getInetAddress();
					
					//cambiamos el ip de Receptor por ip De Emisor
					packet.setIp(ip.getHostAddress());
					
					//enviamos el packete
					ConnToRecieve.send(packet);
					
					//conseguimos el nombre a quin enviamos packete
					nickToRecieve = nickToRecieve.split(" ")[1];
					
					
					
					//escribimos en servidor info de el packete
					gui.refreshDialogWindowServer(getHora()+" "+
												  packet.getNickName()+" ("+
												  socket.getRemoteSocketAddress()+") a "+
												  nickToRecieve+"("+
												  ConnToRecieve.getSocket().getRemoteSocketAddress()+"): "+
												  packet.getMessage()+"\n");
				}
				
				//si el user se disconecta
				if(packet.getTypePacket() == PacketType.DISABLE_USER) {
					
					model.removeUser(packet.getIp()+" "+packet.getNickName());
					
					LinkedList<String> listUsers = new LinkedList<>();
		            for (Map.Entry<String, Connection> users : model.getAllUsersMultiChat().entrySet()) {
		                listUsers.add(users.getKey());
		            }
		            
					//enviamos nueva lista de Ip
		            sendPacketAllUsers(new PacketDate(PacketType.LIST_OF_USERS,listUsers));
		            
					gui.refreshDialogWindowServer("Sali el usuario: "+socket.getRemoteSocketAddress()+"\n");
					
					//cerramos el coneccion
					connection.close();
					
					break;
				}
				
				
			} catch (IOException | ClassNotFoundException e) {
				gui.refreshDialogWindowServer("Problem to sending Packets or to disable User! \n"+e.getMessage() +"\n");
				break;
			}
           }
       }
     
     //consequimos coneccion segun un ip
     public Connection getConnection(String ip) {
    	 for (Map.Entry<String, Connection> user : model.getAllUsersMultiChat().entrySet()) {
             if(user.getKey().equals(ip)) {
           	  return user.getValue();
             }
        }
    	 return null;
     }

     //get Hora para fichero log
     public String getHora() {
    	Calendar hora = Calendar.getInstance(); 
	   	//Formating una data en hora:minutos
	   	String formatHour = hora.get(Calendar.HOUR_OF_DAY)+":";
	   	formatHour = formatHour +hora.get(Calendar.MINUTE);
	   	return formatHour;
   		
   	}

        @Override
        public void run() {
            gui.refreshDialogWindowServer(String.format("New User Connected - %s.\n", socket.getRemoteSocketAddress()));
            try {
                //establecemos coneccion
                Connection connection = new Connection(socket);
                //add el usuario
                String nameUser = requestAndAddingUser(connection);
                //creamos el trhead de escuchar
                messagingBetweenUsers(connection, nameUser);
            } catch (Exception e) {
            	e.printStackTrace();
                gui.refreshDialogWindowServer(String.format("Error to Sending Packet!\n"));
            }
        }
    }
}