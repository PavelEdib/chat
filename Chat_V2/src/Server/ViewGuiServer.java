package Server;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ViewGuiServer {
    private JFrame frame = new JFrame("Server Running...");
    private JTextArea dialogWindow = new JTextArea(10, 40);
    private JButton buttonStartServer = new JButton("Turn on");
    private JButton buttonStopServer = new JButton("Turn off");
    private JPanel panelButtons = new JPanel();
    private File FileLog;
    private final Server server;

    public ViewGuiServer(Server server) {
    	frame.setResizable(false);
        this.server = server;
    }

    
    protected void initFrameServer() {
    	FileLog = new File(getDate()+".log");
    	if(!FileLog.exists()) {
    		try {
				FileLog.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
    	}
        dialogWindow.setEditable(false);
        dialogWindow.setLineWrap(true);  
        frame.getContentPane().add(new JScrollPane(dialogWindow), BorderLayout.CENTER);
        panelButtons.add(buttonStartServer);
        panelButtons.add(buttonStopServer);
        frame.getContentPane().add(panelButtons, BorderLayout.SOUTH);
        frame.pack();
        frame.setLocationRelativeTo(null); 
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                server.stopServer();
                System.exit(0);
            }
        });
        frame.setVisible(true);

        buttonStartServer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                server.startServer(5050);
            }
        });
        buttonStopServer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                server.stopServer();
            }
        });
    }

    public void refreshDialogWindowServer(String serviceMessage) {
        dialogWindow.append(serviceMessage);
        WriteInFile(serviceMessage);
    }
    
    public String getDate() {
    	DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        return dateFormat.format(date);
    }
    
    public void WriteInFile(String serviceMessage) {
    	try (FileWriter f = new FileWriter(FileLog, true);
              BufferedWriter b = new BufferedWriter(f);
              PrintWriter p = new PrintWriter(b);) {

            p.println(serviceMessage);

        } catch (IOException i) {
            i.printStackTrace();
        }
    }
    
    

}