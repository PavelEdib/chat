package Server;

import Utils.Connection;

import java.util.HashMap;
import java.util.Map;

public class ModelGuiServer {
    //Una mapa que guarda el usuario(ip) con su Conexion
    private Map<String, Connection> allUsersMultiChat = new HashMap<>();

    protected Map<String, Connection> getAllUsersMultiChat() {
        return allUsersMultiChat;
    }

    protected void addUser(String nameUser, Connection connection) {
        allUsersMultiChat.put(nameUser, connection);
    }

    protected void removeUser(String nameUser) {
        allUsersMultiChat.remove(nameUser);
    }

}