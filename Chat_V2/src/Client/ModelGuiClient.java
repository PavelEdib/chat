package Client;

import java.util.HashSet;
import java.util.Set;

public class ModelGuiClient {
    //lista de usuarios
    private Set<String> users = new HashSet<>();
    
    
    protected Set<String> getUsers() {
        return users;
    }
    
    //Anadimos un usuario
    protected void addUser(String nameUser) {
        users.add(nameUser);
    }
    
    //Eliminamos un usuario den lista 
    protected void removeUser(String nameUser) {
        users.remove(nameUser);
    }

    
    protected void setUsers(Set<String> users) {
        this.users = users;
    }
}
