package Client;


import java.net.InetSocketAddress;
import java.net.Socket;

import Utils.Connection;
import Utils.PacketDate;
import Utils.PacketType;

public class Client {
	private Connection connection;
    public static ModelGuiClient model;
    private static ViewGuiClient gui;
	public String ipOwn;
	public boolean isConnect=false;
 

	public static void main(String[] args) {
		 Client client = new Client();
	     model = new ModelGuiClient();
	     gui = new ViewGuiClient(client);
	     
	     //metemos el nickname en label
	     String nickname =gui.setNameUser();
	     gui.lblNick.setText(nickname);
	     
	     
	     //el bucle que siempre esta escuchando
		while(true) {	
			
			if(client.isConnect) {
				client.receiveMessageFromServer();
			}else
				System.out.println();
			
		}
 	     
	}
	
	//recibe el pacquete del servidor
	 public void receiveMessageFromServer() {
		 while(true) {
	            try {
	                PacketDate packet = connection.receive();
	                
	                //si es de tipo TEXT_MESSAGE
	                if (packet.getTypePacket() == PacketType.TEXT_MESSAGE) {
	                	String message =packet.getNickName() +"("+packet.getIp()+"): "+packet.getMessage(); 
	                    gui.addMessage(message);
	                }
	                
	                //si es de tipo LIST_OF_USERS
	                if (packet.getTypePacket() == PacketType.LIST_OF_USERS) {
	                    gui.refreshListUsers(packet.getUsersList());   
	                }   
	            } catch (Exception e) {
	                gui.errorDialogWindow("Error en recibir mensaje!! Reconectate!");
	                //gui.refreshListUsers(model.getUsers());
	                gui.comboBoxIp.removeAllItems();
	                isConnect=false;
	                gui.btnConectar.setEnabled(true);
	                break;
	            }
		 }
	        
	    }
	
	 //Registramos online un usuario
    protected void nameUserRegistration() {
	     try {
	    	 //conseguimos nuestro ip global
	    	 Socket socket = new Socket();
	    	 socket.connect(new InetSocketAddress("google.com", 80));
	    	 String OwnIp = socket.getLocalAddress().getHostAddress();
	    	 socket.close();
	    	 //end
	    	 
	    	 //enviamos el paqueste que estamos online
	        connection.send(new PacketDate(PacketType.REQUEST_USER,gui.lblNick.getText(),"@Online",OwnIp));	
		} catch (Exception e) {
			this.isConnect=false;
			gui.errorDialogWindow("Error to register a user!");
		}
    }
	
	//Nos conectamos con Servidor
    protected void connectToServer() {
      
       try {
        //llamamos metodo de din client gui para introdusir un ip
         String addressServer = gui.getServerAddressFromOptionPane();
                    
          //creamos el socket y la conection
         Socket socket = new Socket(addressServer, 5050);
         setConnection(new Connection(socket));
        
         this.isConnect = true;
         gui.addMessage("You have conected to server! \n");
                    
      } catch (Exception e) {
          gui.addMessage("Something went Wrong!!! \n");            
      }

    }
    
    
    
    
    //enviar un paquete de tipo mensaje
    protected void sendMessageOnServer(String message,String name,String ip) {
        try {
            connection.send(new PacketDate(PacketType.TEXT_MESSAGE,name,message,ip));
        } catch (Exception e) {
            gui.errorDialogWindow("Error to send a message!");
        }
    }
    
    //metodo de disable den servidor
    protected void disableClient() {
        try {
        	//para conseguir nuestro Ip(global)
        	 Socket socket = new Socket();
	    	 socket.connect(new InetSocketAddress("google.com", 80));
	    	 String OwnIp = socket.getLocalAddress().getHostAddress();
	    	 socket.close();
	    	 
        	//enviar el packete de offline
            connection.send(new PacketDate(PacketType.DISABLE_USER,gui.lblNick.getText(),"@Offline",OwnIp));
        } catch (Exception e) {
            gui.errorDialogWindow("Error to disconect!");
        }
    }
    
    

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

}
