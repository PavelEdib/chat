package Client;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.LinkedList;

import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JButton;

public class ViewGuiClient extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2919907760990455361L;
	private JPanel contentPane;
	private JTextField txtFMessage;
	public JComboBox<String> comboBoxIp;
	public JLabel lblNick;
	public JTextArea textAreaMessages; 
	private JScrollPane scrollPane;
	public JButton btnConectar;
	 
	public ViewGuiClient(Client client) {
		setResizable(false);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 644, 612);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nick: ");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(10, 10, 61, 36);
		contentPane.add(lblNewLabel);
		
		lblNick = new JLabel("");
		lblNick.setHorizontalAlignment(SwingConstants.LEFT);
		lblNick.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNick.setBounds(81, 10, 151, 36);
		contentPane.add(lblNick);
		
		JLabel lblOnline = new JLabel("Online:");
		lblOnline.setHorizontalAlignment(SwingConstants.RIGHT);
		lblOnline.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblOnline.setBounds(242, 10, 90, 36);
		contentPane.add(lblOnline);
		
		comboBoxIp = new JComboBox<>();
		comboBoxIp.setFont(new Font("Tahoma", Font.BOLD, 20));
		comboBoxIp.setBounds(359, 10, 253, 36);
		contentPane.add(comboBoxIp);
		
		
		textAreaMessages = new JTextArea();
		textAreaMessages.setEditable(false);
		textAreaMessages.setFont(new Font("Yu Gothic", Font.BOLD, 20));
		textAreaMessages.setBounds(30, 72, 533, 400);
		//contentPane.add(textAreaMessages);
		
		txtFMessage = new JTextField();

		txtFMessage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String ip = getIpFromComboBox();
				client.sendMessageOnServer(txtFMessage.getText(),lblNick.getText(),ip);
				addMessage("Yo: "+txtFMessage.getText());
				txtFMessage.setText("");
			}
		});
		scrollPane = new JScrollPane(textAreaMessages);
		scrollPane.setBounds(30, 72, 582, 400);
		contentPane.add(scrollPane);
		
		
		txtFMessage.setFont(new Font("Yu Gothic UI", Font.BOLD, 20));
		txtFMessage.setBounds(30, 482, 582, 36);
		contentPane.add(txtFMessage);
		txtFMessage.setColumns(10);
		
		btnConectar = new JButton("Conectar");
		
		//estabilicemos el conexion
		btnConectar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			     client.connectToServer();
			     client.nameUserRegistration();
			     
			     //si se ha estabilicido un conexion quitamos el button
			     if(client.isConnect) {
			    	 btnConectar.setEnabled(false);
			     }
			     
			     
			}
		});
		btnConectar.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnConectar.setBounds(253, 528, 135, 45);
		contentPane.add(btnConectar);
		
		
		//metodo de serrar aplicacion el la GUI
		this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
            	if(client.isConnect) {
            		client.disableClient();
            	}
                System.exit(0);
            }
        });
		this.revalidate();
		this.repaint();
		
	}
	
	protected void addMessage(String text) {
		textAreaMessages.append(text+"\n");
    }
	
	
	//metemos lista de los usuarios online en la VIEW
	protected void refreshListUsers(LinkedList<String> listUsers) {
		
		String cadena = null;
		//fragmento de eliminar den la lista nustro ip
		try {
			Socket socket = new Socket();
	    	 socket.connect(new InetSocketAddress("google.com", 80));
	    	 String OwnIp = socket.getLocalAddress().getHostAddress();
	    	 socket.close();
			cadena = OwnIp+" "+lblNick.getText();
		} catch (IOException e) {
			//e.printStackTrace();
		}
		comboBoxIp.removeAllItems();
			
	       for(String ip:listUsers) {
	    	   //nos anade el lista GUI todos los usuarios menos a si mismo
	    	   if(!cadena.equalsIgnoreCase(ip)) {
	       		comboBoxIp.addItem(ip);
	    	   }
	       }
	       this.revalidate();
	       this.repaint();
	}
	
	//metodo nos devuelve un String (ip del servidor) cual lo pedimos
    protected String getServerAddressFromOptionPane() {
            String addressServer = JOptionPane.showInputDialog(
                    null, "Introduzca la ip del servidor:",
                    "Server ip",
                    JOptionPane.QUESTION_MESSAGE
            );
            return addressServer.trim();
        
    }
    public String getIpFromComboBox() {
		
		return (String) comboBoxIp.getSelectedItem();
	}
    
    //metodo de introducir un usuario
    protected String setNameUser() {
        return JOptionPane.showInputDialog(
                null, "Introduzca el nombre:",
                "Form de Usuario!",
                JOptionPane.QUESTION_MESSAGE
        );
    }
    
    protected void errorDialogWindow(String text) {
        JOptionPane.showMessageDialog(
                this, text,
                "Error", JOptionPane.ERROR_MESSAGE
        );
    }
}
